//
//  memoApp.swift
//  memo
//
//  Created by 笹平隼佑 on 2022/11/08.
//

import SwiftUI

@main
struct memoApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
